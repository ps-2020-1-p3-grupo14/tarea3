CC= gcc -Wall -fsanitize=address,undefined
obj1=cola
obj2=funciones
bin/cola_exe: obj/$(obj1).o obj/$(obj2).o
	$(CC) $^ -o $@

obj/$(obj1).o: src/$(obj1).c
	$(CC)  $^ -c -I ./include/ -o $@

obj/$(obj2).o: src/$(obj2).c
	$(CC)  $^ -c -I include/ -o $@

.PHONY: clear
clear:
	rm obj/*
	rm bin/*